package mobile.labs.acw;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

public class ScoreFragment extends Fragment
{
    public ScoreFragment()
    {

    }

    public void updateScore(double score)
    {
        /*
        This method updates the displayed score in the fragment
        */

        TextView textView = (TextView) getView().findViewById(R.id.scoreTextView);
        textView.setText(getString(R.string.currentScore) + score);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_score, container, false);                                         // Inflate the layout for this fragment
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
    }

}
