package mobile.labs.acw;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import java.io.File;

public class MainActivity extends AppCompatActivity implements FilterFragment.FilterSelected
{
    String[] m_LocalPuzzles = new String[0], m_RemotePuzzles = new String[0], mAllPuzzles = new String[0];

    public String[] clearDuplicates(String[] localPuzzles, String[] allPuzzles)
    {

        int duplicate = 0;

        for (int i = 0; i < allPuzzles.length; i++)                                     //Find the number of duplicates
            for (int j = 0; j < localPuzzles.length; j++)
                if (allPuzzles[i] == localPuzzles[j]) duplicate++;

        String[] newA = new String[allPuzzles.length-duplicate];                            //New A without duplicates
        int notDuplicate = 0;                                                               //For indexing elements in the new A
        boolean check;                                                                      //For knowing if it is or isn't a duplicate

        for (int i = 0; i < allPuzzles.length; i++)
        {
            check = true;

            for (int j = 0; j < localPuzzles.length; j++) {
                if (allPuzzles[i] == localPuzzles[j]) {
                    check = false;
                    notDuplicate--;                                                     //Adjust index
                }
            }

            if(check) newA[notDuplicate] = allPuzzles[i];                               //Put this element in the new array
            notDuplicate++;                                                             //Adjusting index
        }

        return newA;
    }

    protected void updateFrags()
    {
        /*
        This method gets the two relevant fragments, and calls a method which will find available puzzles, both local and remote,
        and fill the two list view containers with the result.
         */

        removeDuplicates();
        SavedPuzzlesFragment savedPuzzlesFragment = (SavedPuzzlesFragment) getSupportFragmentManager().findFragmentById(R.id.savedPuzzlesFragment);
        OnlinePuzzlesFragment onlinePuzzlesFragment = (OnlinePuzzlesFragment) getSupportFragmentManager().findFragmentById(R.id.onlinePuzzlesFragment);
        savedPuzzlesFragment.setPuzzles(m_LocalPuzzles);
        onlinePuzzlesFragment.setPuzzles(m_RemotePuzzles);
    }

    public void removeDuplicates()
    {
        String remoteEntry = "", localEntry = "";
        int duplicate = 0, notDuplicate = 0;
        boolean matches;

        /*
        Find the number of duplicates in the set, this will allow us to instantiate and return
        an array of appropriate size
        */

        for (String remotePuzzle : m_RemotePuzzles)
        {
            for (String localPuzzle : m_LocalPuzzles)
            {
                remoteEntry = remotePuzzle.substring(0, remotePuzzle.length() - 5);
                localEntry = localPuzzle.substring(0, localPuzzle.length() - 4);
                if (remoteEntry.equals(localEntry)) duplicate++;
            }
        }

        String[] noDuplicates = new String[m_RemotePuzzles.length-duplicate];

        /*
        Now we have instantiated an array to the correct size, we can iterate through again,
        this time filling the new array
        */

        for (String remotePuzzle : m_RemotePuzzles)
        {
            matches = true;

            for (String localPuzzle : m_LocalPuzzles)
            {
                remoteEntry = remotePuzzle.substring(0, remotePuzzle.length() - 5);
                localEntry = localPuzzle.substring(0, localPuzzle.length() - 4);
                if (remoteEntry.equals(localEntry))
                {
                    matches = false;
                    notDuplicate--;
                }
            }

            if (matches) noDuplicates[notDuplicate] = remotePuzzle.substring(0, remotePuzzle.length() - 5) + ".json";
            notDuplicate++;
        }

        m_RemotePuzzles = noDuplicates;
    }

    public void filterChanged(int[] layout)
    {
        /*
        This method finds the relevant saved puzzles when the filter is changed
        */

        m_LocalPuzzles = findDevicePuzzles(layout);
        updateFrags();
    }

    @Override
    public void onResume()
    {
        /*
        Repopulate the frags with the relevant puzzles
        */
        super.onResume();
        m_LocalPuzzles = findDevicePuzzles();
        updateFrags();
        FilterFragment filterFragment = (FilterFragment) getSupportFragmentManager().findFragmentById(R.id.filterFragment);
        filterFragment.resetFilter();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        /*
        This method will create a new thread which finds local and online devices puzzles,
        and assign the result to the respective puzzles member variable.
        */

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        m_LocalPuzzles = findDevicePuzzles();
        populatePuzzleList downloadJson = new populatePuzzleList();                                         //Create a new instance of populatePuzzleList class
        downloadJson.execute();                                                                             //Start the thread
    }

    protected String[] findDevicePuzzles()
    {
        /*
        This method will iterate through the apps save directory, and find the serialised
        bin files which contain puzzle objects, and return an array with the result
        */

        File fileDir;
        String[] localPuzzles = new String[0];
        String availablePuzzles = "";

        try
        {
            fileDir = new File(getBaseContext().getFilesDir() + "/puzzles/");                            //Find the applications save directory
            for (File f : fileDir.listFiles())
            {
                if (f.isFile())
                    availablePuzzles += f.getName() + ",";
            }
            localPuzzles = availablePuzzles.split(",");
        }
        catch (Exception e) { e.printStackTrace(); }
        return localPuzzles;
    }

    protected String[] findDevicePuzzles(int[] layoutDimensions)
    {
        /*
        Overloaded version of the method above, this will only find certain dimension
        puzzles (these are specified in the layoutDimensions array). This array is of
        size two
        */

        if(layoutDimensions[0] == 0 && layoutDimensions[1] == 0) return findDevicePuzzles();

        File fileDir;
        String[] localPuzzles = new String[160];
        String availablePuzzles = "";

        try
        {
            fileDir = new File(getBaseContext().getFilesDir() + "/puzzles/");                            //Find the applications save directory
            for (File f : fileDir.listFiles())
            {
                if (f.isFile())
                {
                    Puzzle puzzle = Puzzle.load(getBaseContext(), f.getName());

                    if (layoutDimensions[0] == puzzle.getWidth() && layoutDimensions[1] == puzzle.getHeight())
                    {
                        availablePuzzles += f.getName() + ",";
                    }
                }
            }
            localPuzzles = availablePuzzles.split(",");
        }
        catch (Exception e) { e.printStackTrace(); }
        return localPuzzles;
    }

    private class populatePuzzleList extends AsyncTask<String,String,String[]>
    {
        /*
        This async task creates a new instance of puzzlehandler, and calls the
        get puzzle list method. This will return a String array which contains
        all the available puzzles on the server. Once executed, we set the member
        variable to the result, remove duplicates, and update the frags.
        */

        @Override
        protected String[] doInBackground(String... strings)
        {
            PuzzleHandler handler = new PuzzleHandler();                        //Create a new instance of PuzzleHandler
            return handler.getPuzzleList();                                     //Call getPuzzleList method, and return puzzle list array
        }

        @Override
        protected void onPostExecute(String[] onlinePuzzles)
        {
            if (onlinePuzzles != null)
            {
                m_RemotePuzzles = onlinePuzzles;
                updateFrags();                                                  //Once both arrays have been filled, update the fragments with this information
            }
            else
            {
                Toast.makeText(getBaseContext(), "Cannot establish connection to server", Toast.LENGTH_SHORT).show();              //If the selected position is not next to an empty cell, display a pop up saying so
            }
        }
    }
}

