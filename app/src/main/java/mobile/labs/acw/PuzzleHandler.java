package mobile.labs.acw;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;

public class PuzzleHandler
{
    private final String m_URL = "http://www.simongrey.net/08027/slidingPuzzleAcw/";                                                    //Fixed m_URL for Simon's server
    private Context m_Context;

    public Puzzle createPuzzle(String puzzleName, Context context)
    {
        /*
        This method will try to load the requested puzzle from storage.
        If the file has not been downloaded before, it will go and retrieve it
        from the server.
        */

        m_Context = context;
        Puzzle puzzle = null;
        String  storedPuzzle = puzzleName.replace(".json",".bin");                                                                      //To get the name of the stored puzzle, replace the json extension with a binary extension
        File f = new File(context.getFilesDir() + "/puzzles/" + storedPuzzle);                                                          //Create a file reference with the current save directory and puzzle name concatenated
        if (f.exists()) puzzle = Puzzle.load(context, storedPuzzle);                                                                    //If the file exists, it has been downloaded before, call the load method to create a new puzzle instance from memory
        else puzzle = downloadPuzzle(puzzleName);
        return puzzle;
    }

    public Puzzle downloadPuzzle(String puzzleName)
    {
        /*
        This method handles all the json, retrieving the puzzle information from
        the server and creating a new Puzzle instance
        */

        HashMap<String, Bitmap> puzzleBitmaps = new HashMap<>();                                                                        //Create a new hash map to store puzzle bitmaps, and keys
        String[][] puzzleLayout;                                                                                                        //Create a new matrix based on the layout size
        JSONObject metaDataObj, layoutObj;                                                                                              //JSON objects, metadata stores picture and layout information strings
        JSONArray innerArray = new JSONArray(), outerArray = new JSONArray();
        String layout = "", pictureSet = "";                                                                                            //Layout and picture strings, will be appended to the base URL to retrieve relevant JSON files
        String URL = "http://www.simongrey.net/08027/slidingPuzzleAcw/";                                                                //Fixed m_URL for Simon's server
        int puzzleWidth = 0, puzzleHeight = 0;

        try
        {
            metaDataObj = getJSONObject(URL + "/puzzles/" + puzzleName);                                                                //Bolt URL together, and retrieve picture set and layout info
            layout = metaDataObj.getString("layout");                                                                                   //Get layout string from metaData object
            pictureSet = metaDataObj.getString("PictureSet");                                                                           //Get picture set string from metaData object
            layoutObj = getJSONObject(URL + "layouts/" + layout);                                                                       //Bolt URL together, and retrieve arrays
            outerArray = layoutObj.getJSONArray("layout");                                                                              //Retrieve the layout array of arrays
            innerArray = outerArray.getJSONArray(0);                                                                                    //Retrieve the first inner array
            puzzleHeight = outerArray.length();                                                                                         //Stores the layout outer array length
            puzzleWidth = innerArray.length();                                                                                          //Stores the layout inner array width
        }
        catch (Exception e) { e.printStackTrace(); }

        puzzleLayout = new String[puzzleHeight][puzzleWidth];                                                                           //Instantiate layout matrix to the correct puzzle size

        try
        {
            for (int i = 0; i < outerArray.length(); i++)                                                                               //For each outer array
            {
                innerArray = outerArray.getJSONArray(i);                                                                                //Store the outer array

                for (int j = 0; j < innerArray.length(); j++)                                                                           //For each inner array
                {
                    String currentItem = innerArray.getString(j);                                                                       //Get each string member from inner array
                    puzzleLayout[i][j] = currentItem;                                                                                   //Gdd string member to layout matrix
                    puzzleBitmaps.put(currentItem, getPuzzleImage(pictureSet + "/" +  currentItem + ".jpg", m_Context));                //Download the bitmap using information above
                }
            }
        }
        catch (Exception e) { return null; }

        return new Puzzle(puzzleName, puzzleLayout, puzzleBitmaps, puzzleHeight, puzzleWidth, pictureSet);                              //Create and return a new instance of puzzle with the information above
    }

    public Bitmap getPuzzleImage(String imageName, Context context)
    {
        /*
        This method will get a single picture from the server, using the passed imageName
        parameter
        */

        Bitmap requestedImage = null;
        File appDirectory = context.getFilesDir();                                                                                      //Get application save directory

        try                                                                                                                             //Try to find the requested picture
        {
            FileInputStream inStream = new FileInputStream(new File(appDirectory + "/images/" + imageName));                            //Initialise an input stream from the image save directory
            requestedImage = BitmapFactory.decodeStream(inStream);                                                                      //Decode the stored data and create a bitmap instance of it
        }
        catch(FileNotFoundException fnf)                                                                                                //If the file is not found, the puzzle has never been loaded
        {
            try                                                                                                                         //Retrieve it from Simon's server
            {
                requestedImage = BitmapFactory.decodeStream((InputStream) new URL(m_URL + "images/" + imageName).getContent());         //Get the image data from the server, decode it and create a bitmap instance of it
                FileOutputStream outStream = null;                                                                                      //Create a new output stream variable

                try
                {
                    String[] imagePath = imageName.split("/");                                                                          //Split the image m_URL
                    File directory = new File(appDirectory + "/images/" + imagePath[0]);                                                //Create a new image save directory
                    directory.mkdirs();
                    File image = new File(imagePath[1]);                                                                                //Create a file instance with the image name, from the split m_URL above
                    outStream = new FileOutputStream(appDirectory + "/images/" + imageName);                                            //Initialise an output stream to the image save directory
                    requestedImage.compress(Bitmap.CompressFormat.JPEG, 100, outStream);                                                //Convert the image to a jpeg, and write it to the image save directory
                }
                catch (Exception e) {}
                finally { outStream.close(); }                                                                                          //Close the file
            }
            catch (Exception e) { e.printStackTrace(); }
        }

        return requestedImage;                                                                                                          //Return the requested image
    }

    public String[] getPuzzleList()
    {
        /*
        This method will retrieve the entire puzzle list from the remote server, and return
        an array with all the entries
        */

        String[] puzzleArray;
        PuzzleHandler handler = new PuzzleHandler();                                                                                    //Create a new instance of puzzle handler
        JSONArray puzzleJSONArray = handler.getJSONArray("http://www.simongrey.net/08027/slidingPuzzleAcw/index.json", "PuzzleIndex");  //Get the JSON array from Simon's server

        try
        {
            puzzleArray = new String[puzzleJSONArray.length()];                                                                    //Create a string array of equal size to store puzzle names
            for (int i = 0; i < puzzleJSONArray.length(); ++i)                                       //Copy each item from the JSON array to a standard array
                puzzleArray[i] = puzzleJSONArray.get(i).toString();
        }
        catch (Exception e)
        {
            return null;
        }

        return puzzleArray;                                                                                                             //Return the array
    }

    public JSONArray getJSONArray(String URL, String arrayName)
    {
        /*
        This method will get a JSON array from a specified URL
        */

        try
        {
            JSONObject object = getJSONObject(URL);                                    //retrieve the JSON object
            return object.getJSONArray(arrayName);                                     //return the desired array from JSON object
        }
        catch (Exception e) { e.printStackTrace(); }

        return null;
    }

    public JSONObject getJSONObject(String URL)
    {
        /*
        This method will get a JSON object from a specified URL
        */

        String result = "", line = "";

        try
        {
            InputStream stream = (InputStream)new URL(URL).getContent();                                                                //Initialise an input stream from the URL
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));                                                  //Create a buffered reader from the stream

            while(line != null)                                                                                                         //While there are lines left to read
            {
                result += line;                                                                                                         //Add current line to result
                line = reader.readLine();
            }

            return new JSONObject(result);                                                                                              //Instantiate and return a new JSON object with the result
        }
        catch (Exception e) { e.printStackTrace(); }

        return null;
    }
}

