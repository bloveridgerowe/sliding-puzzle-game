package mobile.labs.acw;

import android.content.Context;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;

public class ScoreHandler
{
    public HashMap<String, String> load(Context context)
    {
        /*
        This method loads the high score csv file from local storage
        */

        HashMap<String, String> highScoreMap = new HashMap<>();
        String[] fileArray = new String[0];
        String fileString = "", line = "";
        InputStream inputStream;
        InputStreamReader inputStreamReader;
        BufferedReader bufferedReader;

        try
        {
            inputStream = context.openFileInput("scores.csv");

            if (inputStream != null)
            {
                inputStreamReader = new InputStreamReader(inputStream);
                bufferedReader = new BufferedReader(inputStreamReader);
                while ((line = bufferedReader.readLine()) != null) fileString += line;
                inputStream.close();
            }

            fileArray = fileString.split(",");
            for (int i = 0; i < fileArray.length; ++i) highScoreMap.put(fileArray[i], fileArray[++i]);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return highScoreMap;
    }

    public void save(Context context, HashMap<String, String> highScores)
    {
        /*
        This method saves the high scores to local storage
        */

        try
        {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("scores.csv", Context.MODE_PRIVATE));
            for (Map.Entry<String, String> entry : highScores.entrySet()) outputStreamWriter.write(entry.getKey() + "," + entry.getValue() + ",");
            outputStreamWriter.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}


