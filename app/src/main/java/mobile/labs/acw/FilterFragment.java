package mobile.labs.acw;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class FilterFragment extends Fragment
{
    FilterSelected m_Callback;                                                       //used to callback to the main activity
    String[] m_PuzzleLayouts = new String[]{"All","3x3","3x4","4x3","4x4"};          //stores the possible puzzle layouts

    public interface FilterSelected
    {
        public void filterChanged(int[] layout);
    }

    public void resetFilter()
    {
        /*
        This method resets the filter spinner to "all"
        */

        Spinner spinner = (Spinner) getView().findViewById(R.id.layoutSpinner);
        spinner.setSelection(0);
    }

    public FilterFragment()
    {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_filter, container, false);
        Spinner spinner = (Spinner) view.findViewById(R.id.layoutSpinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String> (getContext(),android.R.layout.simple_list_item_1, android.R.id.text1, m_PuzzleLayouts);         //Create an array adaptor to link the puzzle array to the list view object
        spinner.setAdapter(adapter);

        /*
        The listener below will return an array with two members, which correlate
        to the layout of the requested puzzles (int[]{3,4} -> 3x4 puzzle)
        */

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l)
            {
                int[] returnArray = new int[2];
                switch (pos)
                {
                    case 1:
                        returnArray = new int[]{3,3};
                        break;
                    case 2:
                        returnArray = new int[]{3,4};
                        break;
                    case 3:
                        returnArray = new int[]{4,3};
                        break;
                    case 4:
                        returnArray = new int[]{4,4};
                        break;
                    default:
                        returnArray = new int[]{0,0};
                        break;
                }

                m_Callback.filterChanged(returnArray);                           //invoke the callback with the selected puzzle size array
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView)
            {

            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        /*
        This checks if the callback interface has been implemented
        */

        if (context instanceof FilterFragment.FilterSelected) m_Callback = (FilterFragment.FilterSelected) context;
        else throw new RuntimeException(context.toString() + "must implement filter selected");
    }
}
