package mobile.labs.acw;

import android.content.Context;
import android.graphics.Bitmap;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;

/*
Implementing the serializable interface allows the objects contents to be serialised to binary, and exported.
The puzzle pictures hash map is made transient to exclude it from serialisation during saving, images will be
saved separately to avoid duplication
*/

public class Puzzle implements Serializable
{
    private String m_Name;
    private String m_PictureSetName;
    private String[][] m_PuzzleLayout;
    private transient HashMap<String, Bitmap> m_PuzzlePictures;
    private int m_Width, m_Height;

    public Puzzle(String name, String[][] puzzleLayout, HashMap<String,Bitmap> puzzlePictures, int puzzleHeight, int puzzleWidth, String pictureSetName)
    {
        /*
        This class stores all the required information to build a puzzle
        */

        m_Name = name;
        m_PuzzleLayout = puzzleLayout;
        m_PuzzlePictures = puzzlePictures;
        m_Height = puzzleHeight;
        m_Width = puzzleWidth;
        m_PictureSetName = pictureSetName;
    }

    public void save(Context context)
    {
        /*
        This method will save the current puzzle instance to a puzzle folder in the apps
        local save directory.
        */

        try
        {
            String saveName = m_Name.split(".json")[0] + ".bin";                                                    //Retrieve the puzzle json name, and create the save file name by replacing the extension
            File appDirectory = context.getFilesDir();                                                              //Find the applications save directory
            File puzzleDirectory = new File(appDirectory + "/puzzles");                                             //Create a new puzzle save directory
            puzzleDirectory.mkdirs();                                                                               //Make the directories if necessary
            FileOutputStream outStream = new FileOutputStream(appDirectory + "/puzzles/" + saveName);               //Initialise an output stream to the puzzle save directory
            ObjectOutputStream objOutStream = new ObjectOutputStream(outStream);                                    //Initialise an object output stream to write the puzzle instance out
            objOutStream.writeObject(this);                                                                         //Save the puzzle
            objOutStream.close();                                                                                   //Close the file
            outStream.close();                                                                                      //Close the file
        }
        catch(Exception e) { e.printStackTrace(); }
    }

    public static Puzzle load(Context context, String puzzleName)
    {
        /*
        This load method is made static to allow calling from outside an instance to load the object
        */

        Puzzle puzzle = null;                                                                                       //Initialise an empty puzzle object
        File fileDir;
        FileInputStream inStream;
        ObjectInputStream objInStream;
        HashMap<String, Bitmap> pictureSet = new HashMap<String, Bitmap>();                                                      //Create a hash map to store the picture set to be read in (images stored separately from the .bin object files)
        PuzzleHandler puzzleHandler = new PuzzleHandler();                                                          //Create a new instance of puzzle handler

        try
        {
            fileDir = context.getFilesDir();                                                                        //Find the applications save directory
            inStream = new FileInputStream(new File(fileDir + "/puzzles/" + puzzleName));                           //Initialise an input stream from the puzzle save directory
            objInStream = new ObjectInputStream(inStream);                                                          //Initialise an object input stream to read the saved binary puzzle instance
            puzzle = (Puzzle) objInStream.readObject();                                                             //Read the object in, and cast it to a puzzle
            objInStream.close();                                                                                    //Close the file
            inStream.close();                                                                                       //Close the file
            String[][] puzzleLayout = puzzle.getPuzzleLayout();                                                     //Initialise a string matrix and store the puzzles layout

            for (String[] row : puzzleLayout)
            {
                for (String element : row)
                {
                    if (element.equals("empty")) { pictureSet.put(element, null); }                                 //If we have reached the empty segment, add a null object to the picture set hash map
                    else { pictureSet.put(element, puzzleHandler.getPuzzleImage(puzzle.getPictureSetName() + "/" + element + ".jpg", context)); }        //Else, get the picture (either from memory, or download if not) and add it to the set
                }
            }

            puzzle.setPictureSet(pictureSet);                                                                       //Set the puzzle objects picture set
        }
        catch (Exception e) { e.printStackTrace(); }
        return puzzle;
    }
    
    public String getPictureSetName()
    {
        return m_PictureSetName;
    }

    public String getName()
    {
        return m_Name;
    }

    public int getWidth()
    {
        return m_Width;
    }

    public int getHeight()
    {
        return m_Height;
    }

    public String[][] getPuzzleLayout()
    {
        return m_PuzzleLayout;
    }

    public void setPuzzleLayout(int rowPos, int columnPos, String newValue)
    {
        m_PuzzleLayout[rowPos][columnPos] = newValue;
    }

    public void setPuzzleLayout(String[][] newArray)
    {
        m_PuzzleLayout = newArray;
    }

    public HashMap<String,Bitmap> getPictureSet()
    {
        return m_PuzzlePictures;
    }

    public void setPictureSet(HashMap<String,Bitmap> puzzlePictures)
    {
        m_PuzzlePictures = puzzlePictures;
    }

}
