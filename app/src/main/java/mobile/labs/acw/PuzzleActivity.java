package mobile.labs.acw;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.io.File;
import java.util.HashMap;

public class PuzzleActivity extends AppCompatActivity implements PuzzleFragment.updateScore
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        /*
        In this method, we download the puzzle information and create a new puzzle instance
        from that data.
        */

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_puzzle);
        Bundle bundle = getIntent().getExtras();                                                                                //Get bundle, which contains any putExtra() data
        downloadPuzzleInfo dpi = new downloadPuzzleInfo();                                                                      //Create new downloader object
        dpi.execute(bundle.getString("puzzleName"));                                                                            //Start the thread, with the puzzleName string in putExtra() bundle
    }

    public void sendText(double score)
    {
        ScoreFragment scoreFragment = (ScoreFragment) getSupportFragmentManager().findFragmentById(R.id.gameStatusFragment);
        scoreFragment.updateScore(score);
    }

    private class downloadPuzzleInfo extends AsyncTask<String,String,Puzzle>                                                    //All puzzle info must be retrieved here
    {
        @Override
        protected Puzzle doInBackground(String... strings)
        {
            /*
            This method will load the puzzle, either from memory (if available), or from
            the remote server. If the puzzle file has not been saved before, it will be saved.
            */

            PuzzleHandler puzzleHandler = new PuzzleHandler();                                                                  //Create new JSON handler object
            Puzzle puzzle =  puzzleHandler.createPuzzle(strings[0], getBaseContext());                                          //Retrieve puzzle, with the puzzle m_Name passed from onCreate()
            File puzzleFile = new File(getBaseContext().getFilesDir() + "/puzzles/" + strings[0].replace(".json",".bin"));      //Create a file directory
            if (!puzzleFile.exists()) puzzle.save(getBaseContext());                                                            //If the puzzle is not already cached (does not exist), save it
            return puzzle;                                                                                                      //Return the new puzzle instance
        }

        @Override
        protected void onPostExecute(Puzzle result)
        {
            /*
            In this method, we send the puzzle instance to the frag, so it can display the retrieved puzzle
            */

            PuzzleFragment puzzleFrag = (PuzzleFragment) getSupportFragmentManager().findFragmentById(R.id.puzzleFragment);     //Retrieve the fragment
            puzzleFrag.initialiseGrid(result);                                                                                  //Initialise the grid view with the passed puzzle
        }
    }
}