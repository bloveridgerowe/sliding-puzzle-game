package mobile.labs.acw;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.HashMap;

/**
 * Created by Ben on 15/03/2018.
 */

public class OnlinePuzzlesFragment extends Fragment
{
    String[] m_RawPuzzles = new String[0];
    String[] m_PrettyPuzzles = new String[0];

    public OnlinePuzzlesFragment()
    {

    }

    public void setPuzzles(final String[] puzzles)
    {
        /*
        This method populates the local puzzles list view with available stored games.
        A listener is bound to the list view, which will launch a new activity with the
        selected puzzle.
        */

        m_RawPuzzles = puzzles;
        m_PrettyPuzzles = parseArray(m_RawPuzzles);

        ListView listView = (ListView) getView().findViewById(R.id.onlineListView);
        ArrayAdapter<String> adapter = new ArrayAdapter<String> (getContext(),android.R.layout.simple_list_item_1, android.R.id.text1, m_PrettyPuzzles);  //Create an array adaptor to link the puzzle array to the list view object
        listView.setAdapter(adapter);                                                                                                               //Bind the array and the list view
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l)
            {                                                                             //Bind a click listener to the listView container
                Intent intent = new Intent(view.getContext(), PuzzleActivity.class);                                                                //Create an intent, specifying the PuzzleActivity class
                String puzzleName = m_RawPuzzles[pos];                                                                                               //Get the puzzle name of the selected item
                intent.putExtra("puzzleName",puzzleName);                                                                                           //Add the puzzle name to putExtra, to inform the PuzzleActivity class of requested puzzle
                startActivity(intent);                                                                                                              //Start the new puzzle activity
            }
        });
    }

    protected String[] parseArray(String[] preClean)
    {
        /*
        This method cleans up the raw array, making it presentable for the list view.
        */

        String[] postClean = new String[preClean.length];                                               //Create a new array of equal length

        for (int i = 0; i < preClean.length; i++)
        {
            String s = preClean[i];                                                                     //Get the current entry from the array
            s = s.replaceAll("([^0-9\\-\\s])(-?\\d\\.?\\d*)", "$1 $2");                                 //Regex to add a space before puzzle number
            s = s.substring(0,s.length()-5);                                                            //Chop off the .json extension
            s = s.substring(0, 1).toUpperCase() + s.substring(1);                                       //Capitalise the 'p' in puzzle
            postClean[i] = s;                                                                           //Add the cleaned string to the new array
        }

        return postClean;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_online_puzzles, container, false);
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
    }

}
