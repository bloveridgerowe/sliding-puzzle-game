package mobile.labs.acw;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;
import android.preference.PreferenceManager;

import java.io.File;
import java.util.Calendar;
import java.util.HashMap;

public class PuzzleFragment extends Fragment
{
    private GridView m_GridView = null;
    private Context m_Context = null;
    private Puzzle m_Puzzle = null;
    private int m_NoOfMoves = 1;
    private double m_Score;
    protected updateScore m_Callback;
    HashMap<String, String> m_HighScore = new HashMap<>();
    private long m_SessionTime = 0, m_StartTime = 0;


    public PuzzleFragment()
    {

    }

    public void getHighScores()
    {
        /*
        This method will try to load the high scores csv file from local storage.
        If it does not exist, it will be created.
        */

        ScoreHandler scoreHandler = new ScoreHandler();
        File scoreFile = new File(getContext().getFilesDir() + "/scores.csv");                                                  //Create a file directory
        if (!scoreFile.exists()) scoreHandler.save(m_Context, m_HighScore);
        else m_HighScore = scoreHandler.load(getContext());
    }

    public void setHighScores()
    {
        /*
        This method will export the hash map to local storage, for persistence.
        */

        ScoreHandler scoreHandler = new ScoreHandler();
        scoreHandler.save(m_Context, m_HighScore);
    }

    public void initialiseGrid(Puzzle puzzle)
    {
        /*
        This method will initialise the puzzle grid, and restore any relevant member variables
        to enable application save persistence.
        */

        getHighScores();
        m_GridView.setNumColumns(puzzle.getWidth());
        m_Puzzle = puzzle;
        restoreState();                                                                                                         //If the puzzle has been played previously, restore its state
        drawGrid();                                                                                                             //Draw the puzzle grid
        generateScore();
        m_GridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {                                           //Create an listener to detect a tap
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
            int[] emptyCoordinate = findEmptyCoordinate();
            int[] selectedCoordinate = getGridCoords(pos);                                                                      //Get selected coordinate by parsing the button index that had been tapped

            if (legalMove(selectedCoordinate, emptyCoordinate)) swapTiles(emptyCoordinate, selectedCoordinate);                 //If the selected position is next to the empty cell, swap tiles
            else Toast.makeText(getContext(), R.string.illegalMove, Toast.LENGTH_SHORT).show();                                       //If the selected position is not next to an empty cell, display a pop up saying so
            }
        });
    }

    public void calculateGameSeconds()
    {
        /*
        This method will calculate the duration of play for the current puzzle.
        If the orientation changes, add the previous score to the new (reset) score.
        */

        long currentTime = Calendar.getInstance().getTime().getTime();
        m_SessionTime = (currentTime - m_StartTime) / 1000;
    }

    public void swapTiles(int[] emptyCoordinate, int[] selectedCoordinate)
    {
        /*
        This method is responsible for user interaction with the puzzle grid.
        It will check if the move is legal (detailed in the game complete method below)
        */

        calculateGameSeconds();
        generateScore();
        m_NoOfMoves++;
        String currentPictureName = m_Puzzle.getPuzzleLayout()[selectedCoordinate[0]][selectedCoordinate[1]];                    //Get the current picture name
        m_Puzzle.setPuzzleLayout(selectedCoordinate[0], selectedCoordinate[1], m_Puzzle.getPuzzleLayout()[emptyCoordinate[0]][emptyCoordinate[1]]);        //Set selected cell value to "empty"
        m_Puzzle.setPuzzleLayout(emptyCoordinate[0], emptyCoordinate[1], currentPictureName);                                    //Set the former empty cell image name to the swapped one
        drawGrid();                                                                                                             //Redraw the grid

        if (gameComplete())                                                                                                     //Check if gameComplete is complete
        {
            Toast.makeText(getContext(), R.string.gameComplete, Toast.LENGTH_SHORT).show();                         //Display a message
            setHighScores();
            clearStoredPrefs();
        }
    }

    public int[] findEmptyCoordinate()
    {
        for (int i = 0; i < m_GridView.getChildCount(); i++)                                                        //Find the empty coordinate
        {
            int[] currentCoordinate = getGridCoords(i);                                                             //Parse the array index into a coordinate
            String currentImage = m_Puzzle.getPuzzleLayout()[currentCoordinate[0]][currentCoordinate[1]];           //Get the current image name
            if (currentImage.equals("empty")) return currentCoordinate;                                            //If the current image is called "empty", set the empty coord
        }
        return null;
    }

    public void clearStoredPrefs()
    {
        /* Remove all stored puzzle data in shared preferences */

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getContext());                               //Get a reference to the shared preferences
        SharedPreferences.Editor sharedPreferences = settings.edit();                                                           //Create an preferences editor
        sharedPreferences.clear();
        sharedPreferences.apply();
        m_Score = m_NoOfMoves = 0;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onPause()
    {
        /*
        If the game is interrupted, calculate the current game time, and save the state of the puzzle
        */

        super.onPause();
        if (!gameComplete()) saveState();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if(m_Puzzle != null) restoreState();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_puzzle, container, false);                                           //Inflate the layout for the puzzle fragment
        m_GridView = (GridView)view.findViewById(R.id.puzzleGrid);                                                          //Get a reference to the grid view
        return view;
    }

    private boolean gameComplete()
    {
        /*
        To find if a puzzle is complete, we can iterate through the puzzle matrix and check if the numbers are in ascending order by comparing them to the
        matrix's index. As the puzzle picture names are indexed from 1, not 0, we need to add one to the row and column matrix value to ensure successful comparison
        */

        for (int i = 0; i < m_Puzzle.getPuzzleLayout().length; i++)                                              //For each row in the puzzle
        {
            for (int j = 0; j < m_Puzzle.getPuzzleLayout()[i].length; j++)                                       //For each column in the puzzle
            {
                String imageName = Integer.toString(j + 1) + Integer.toString(i + 1);                           //The json index values start at 1,1 and the loop starts at 0, we must incremement each and create a the image name string

                if (!m_Puzzle.getPuzzleLayout()[i][j].equals(imageName) &&                                       //If the two do not match...
                        !m_Puzzle.getPuzzleLayout()[i][j].equals("empty"))                                       //...and the current cell is not empty, the puzzle is not solved
                            return false;
            }
        }

        if(m_HighScore.get(m_Puzzle.getName()) != null)                                                           //if there is already an entry
        {
            if(m_Score > Double.parseDouble(m_HighScore.get(m_Puzzle.getName())))                                  //if new score is above entry
            {
                m_HighScore.put(m_Puzzle.getName(), Double.toString(m_Score));                                     //add new score
            }
        }
        else
        {
            m_HighScore.put(m_Puzzle.getName(), Double.toString(m_Score));                                         //add new score
        }

        return true;                                                                                            //If the loop continues to the end without error, the puzzle is solved
    }

    public void saveState()
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getContext());               //Get a reference to the shared preferences
        SharedPreferences.Editor sharedPreferences = settings.edit();                                           //Create an preferences editor
        String puzzleImages = "";                                                                               //Stores the puzzle image names

        /* Iterate through the matrix, append each element to the string */

        for (int i = 0; i < m_Puzzle.getHeight(); ++i)
            for (int j = 0; j < m_Puzzle.getWidth(); ++j)
                puzzleImages += m_Puzzle.getPuzzleLayout()[i][j] + ",";

        /* Add all necessary puzzle metadata to the shared preferences for later use */

        calculateGameSeconds();

        sharedPreferences.putString("Name", m_Puzzle.getName());
        sharedPreferences.putString("Pictures", puzzleImages);
        sharedPreferences.putInt("Height", m_Puzzle.getHeight());
        sharedPreferences.putInt("Width", m_Puzzle.getWidth());
        sharedPreferences.putLong("Time", m_SessionTime);
        sharedPreferences.putInt("Moves", m_NoOfMoves);
        sharedPreferences.apply();
    }

    public void restoreState()
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getContext());
        String savedPuzzleName =  settings.getString("Name", "");

        /*
        If the puzzle name stored in shared preferences matches the current puzzle, retrieve the puzzle metadata. The picture set string is converted to
        an array using the String.split method, allowing each element to be iterated through. Using a nested for loop provides two indexes which correspond
        to the position of each tile in the grid. Adding each picture element back into the puzzle member allows full reconstruction of the saved gameComplete state.
         */

        if (savedPuzzleName.equals(m_Puzzle.getName()))                                                      //If a session is stored in shared preferences
        {
            int height = 0, width = 0, count = 0;
            String pictureSetStr;
            String[] pictureSetArr;

            pictureSetStr = settings.getString("Pictures", "");
            height = settings.getInt("Height", 0);
            width = settings.getInt("Width", 0);
            pictureSetArr = pictureSetStr.split(",");
            m_SessionTime = settings.getLong("Time",0);
            m_NoOfMoves = settings.getInt("Moves",0);

            for (int i = 0; i < height; ++i)
            {
                for (int j = 0; j < width; ++j)
                {
                    m_Puzzle.setPuzzleLayout(i, j, pictureSetArr[count]);
                    count++;
                }
            }

            m_StartTime = 0 - (m_SessionTime * 1000) + Calendar.getInstance().getTime().getTime();
        }
        else
        {
            m_StartTime = Calendar.getInstance().getTime().getTime();
        }
    }

    public void generateScore()
    {
        /*
        This method will generate the users current score. To differentiate easy and difficult puzzles,
        the width and height of the puzzle are multiplied to give a higher start value
        */

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getContext());
        String savedPuzzleName =  settings.getString("Name", "");
        calculateGameSeconds();
        m_Score = 200 * (m_Puzzle.getWidth() * m_Puzzle.getHeight()) - (m_SessionTime * m_NoOfMoves);
        if(m_Score <= 0) m_Score = 0;
        m_Callback.sendText(m_Score);
    }

    public void drawGrid()
    {
        /*
        This method creates a new custom image adaptor, and applies it to the grid view
        */

        ImageAdapter imageAdapter = new ImageAdapter(m_Context, m_Puzzle.getPictureSet(), m_Puzzle.getPuzzleLayout());             //Create a new image adapter object
        m_GridView.setAdapter(imageAdapter);                                                                                     //Set the adaptor
    }

    public int[] getGridCoords(int position)
    {
        /*
        This method will convert a raw array position to the 2d matrix equivalent
        */

        int row = position / m_Puzzle.getWidth();                                                                                //Due to the nature of int always rounding down, we can deduce the current row by dividing by the width
        int col = position % m_Puzzle.getWidth();                                                                                //By mod'ing the position index, we can deduce the column by finding the remainder
        return new int[] {row,col};                                                                                             //Return a complete integer array of puzzle picture coordinates
    }

    public boolean legalMove(int[] selectedPosition, int[] emptyPos)
    {
        /*
        To calculate whether a move is legal or not, we need to know where the empty square is, and if the chosen square is immediately adjacent to it
        Subtracting the users selected square coordinates from the empty square coordinates, will give us a translation coordinate
        If the x OR y value is >1 OR <-1, we know the two squares are NOT adjacent to each other, and the move is illegal
         */

        int deltaX = selectedPosition[0] - emptyPos[0];
        int deltaY = selectedPosition[1] - emptyPos[1];

        if (deltaX == 0 && (deltaY == -1 || deltaY == 1)) return true;
        else if (deltaY == 0 && (deltaX == -1 || deltaX == 1)) return true;
        else return false;
    }

    public interface updateScore
    {
        /*
        This interface is used to callback to the puzzle activity, when the
        score needs updating in the score fragment.
        */

        public void sendText(double score);
    }

    @Override
    public void onAttach(Context context)
    {
        /*
        Ensure the current instance implements the updateScore interface
        */

        super.onAttach(context);
        m_Context = context;
        if (context instanceof updateScore) m_Callback = (updateScore) context;
        else throw new RuntimeException(context.toString() + "must implement updateScore");
    }

    @Override
    public void onDetach()
    {
        m_Callback = null;
        super.onDetach();
    }

    public class ImageAdapter extends BaseAdapter
    {
        /*
        This image adapter will populate the grid view with the puzzle images
        */

        private Context m_Context;
        private HashMap<String, Bitmap> m_Images;
        private String[][] m_LayoutData;
        private int m_Col = 0, m_Row = 0;

        public void setImages(HashMap<String, Bitmap> hashMap, String[][] layoutData)                                           //Sets the images and layout data for the custom ImageAdapter
        {
            m_Images = hashMap;
            m_LayoutData = layoutData;
        }

        public ImageAdapter(Context context, HashMap<String,Bitmap> images, String[][] layout)
        {
            m_Context = context;
            m_Images = images;
            m_LayoutData = layout;
        }

        public int getCount() { return m_Images.size(); }

        public Object getItem(int position) { return null; }

        public long getItemId(int position)
        {
            return 0;
        }

        public View getView(int position, View convertView, ViewGroup parent)
        {
            /*
            This method will be called multiple times, increasing the passed
            position parameters each time. We set each tile to the respective bitmap,
            which will generate a picture grid.
            */

            ImageView imageView;

            if (convertView == null)
            {
                imageView = new ImageView(m_Context);
                int gridWidth =  m_GridView.getWidth();
                int gridHeight = m_GridView.getHeight();
                int pictureSize;

                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) pictureSize = gridHeight / m_Puzzle.getHeight();
                else pictureSize = gridWidth / m_Puzzle.getWidth();

                m_GridView.setColumnWidth(pictureSize);
                imageView.setLayoutParams(new GridView.LayoutParams(pictureSize, pictureSize));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }
            else imageView = (ImageView)convertView;

            m_Col = position % m_Puzzle.getWidth();                                                                                  // Determine if need to move to next row
            if (m_Col == 0 && position != 0) m_Row++;                                                                               // Increase column value
            Bitmap image = m_Images.get(m_LayoutData[m_Row][m_Col]);
            imageView.setImageBitmap(image);
            return imageView;
        }
    }
}